focus_tree = {
	id = sweden_focus
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SWE
		}
	}
	default = no
	
	
	focus = {
		id = NDA_resource5
		icon = GFX_goal_generic_consumer_goods
		x = 0
		y = 1
		cost = 195.4
		available_if_capitulated = yes
		completion_reward = {
			412 = {add_state_modifier = {
					modifier = {
						state_production_speed_buildings_factor = 0.30
					}
				}
			}
			every_country={
				limit={is_playable_country=yes}
			if = {
				limit={NOT={has_idea=GER_africa_hp}}
				add_ideas = GER_africa_hp
				set_technology = { africa_hp = 1 popup = no }
			}
				
			}
		}
	}
	
	focus = {
		id = NDA_resource3
		icon = GFX_goal_generic_consumer_goods
		x = 0
		y = 1
		cost = 52
		prerequisite = { focus = NDA_resource5 }
		relative_position_id = NDA_resource5
		available_if_capitulated = yes
		completion_reward = {
			#GER ={remove_ideas = GER_africa_hp}
			#GER ={set_technology = { africa_hp_off = 1 popup = no }}
			every_country={
				limit={is_playable_country=yes}
				remove_ideas = GER_africa_hp
				set_technology = { africa_hp_off = 1 popup = no }
				
			}
		}
	}
	
}

